import starWarsConsumer from "../consumer/starWarsConsumer.js";
import starWarsParser from "./starWarsParser.js";

// STAR WARS VIEW

// IMPORTANT: IMPLEMENT A CATCH BLOCK FOR EVERY CONSUMER FUNCTION

// TASK: Get planets data from starWarsConsumer and parse them to get the name of each planet with starWarsParser, then show the result in the console. TODO: **DEV 1**
async function viewPlanetsName() {
  const planet = await starWarsConsumer.getPlanets();
  const planetNames = starWarsParser.getPlanetsNames(planet);
  console.log('Planet names:');
  console.log(planetNames);
}

// TASK: Get planets data from starWarsConsumer and parse them to get the rotation period of each planet with starWarsParser, then show the result in the console. TODO: **DEV 2**
async function viewPlanetsRotationPeriod() {
  const planets = await starWarsConsumer.getPlanets()
  const planetsAndRotation = starWarsParser.getPlanetsRotationPeriod(planets)
  console.log(planetsAndRotation)
}

// TASK: Get single planet data from starWarsConsumer and parse it to get its terrain with starWarsParser, then show the result in the console. TODO: **DEV 3**
async function viewSinglePlanetTerrain(planetId) {
  const planet = await starWarsConsumer.getSinglePlanet(planetId);
  const planetTerrain = starWarsParser.getSinglePlanetTerrain(planet);
  console.log(`Name: ${planetTerrain.name}`);
  console.log(`Terrain: ${planetTerrain.terrain}`);
}

// TASK: Get people data from starWarsConsumer and parse them to get the name of each person with starWarsParser, then show the result in the console. TODO: **DEV 4**
async function viewPeopleNames() {
  const people = await starWarsConsumer.getPeople();
  const peopleNames = starWarsParser.getVehiclesNames(people);
  console.log('People names:');
  console.log(peopleNames);
}

// TASK: Get people data from starWarsConsumer and parse them to get the height of each person with starWarsParser, then show the result in the console. TODO: **DEV 1**

async function viewPeopleHeights() {
  const people = await starWarsConsumer.getPeople();
  const peopleHeight = starWarsParser.getPeopleHeights(people);
  console.log('People heights:');
  console.log(peopleHeight);
}


// TASK: Get single person data from starWarsConsumer and parse it to get its hair color with starWarsParser, then show the result in the console. TODO: **DEV 2**
async function viewSinglePersonHairColor(personId) {
  const person = await starWarsConsumer.getSinglePerson(personId)
  const PersonAndHair = starWarsParser.getSinglePersonHairColor(person)
  console.log(`nome: ${PersonAndHair.name}`)
  console.log(`Cor do cabelo: ${PersonAndHair.hairColor}`)
}

// TASK: Get films data from starWarsConsumer and parse them to get the title of each film with starWarsParser, then show the result in the console. TODO: **DEV 3**
async function viewFilmsTitles() {
  const films = await starWarsConsumer.getFilms();
  const filmsTitles = starWarsParser.getFilmsTitles(films);
  console.log('Films titles:');
  console.log(filmsTitles);
}

// TASK: Get single film data from starWarsConsumer and parse it to get its opening crawl
// with starWarsParser, then show the result in the console. TODO: **DEV 4**
async function viewSingleFilmOpeningCrawl(filmId) {
  console.log(starWarsParser.getSingleFilmOpeningCrawl(await starWarsConsumer.getSingleFilm(filmId)))

}

// TASK: Get species data from starWarsConsumer and parse them to get the name of each species with starWarsParser, then show the result in the console. TODO: **DEV 1**
async function viewSpeciesNames() {
  const species = await starWarsConsumer.getSpecies();
  const speciesName = await starWarsParser.getSpeciesNames(species);
  console.log(speciesName);
}
// TASK: Get single species data from starWarsConsumer and parse it to get its language with starWarsParser, then show the result in the console. TODO: **DEV 2**

async function viewSingleSpeciesLanguage(speciesId) {
  const specie = await starWarsConsumer.getSingleSpecies(speciesId)
  const specieAndLanguage = starWarsParser.getSingleSpeciesLanguage(specie)
  console.log(`nome: ${specieAndLanguage.name}`)
  console.log(`Linguagem: ${specieAndLanguage.language}`)
}

// TASK: Get vehicles data from starWarsConsumer and parse them to get the name of each vehicle with starWarsParser, then show the result in the console. TODO: **DEV 3**
async function viewVehiclesNames() {
  const vehicles = await starWarsConsumer.getVehicles();
  const vehiclesNames = starWarsParser.getVehiclesNames(vehicles);
  console.log('Vehicles names:');
  console.log(vehiclesNames);
}

// TASK: Get single vehicle data from starWarsConsumer and parse it to get its model with starWarsParser, then show the result in the console. TODO: **DEV 4**
async function viewSingleVehicleModel(vehicleId) {
  console.log(starWarsParser.getSingleVehicleModel(await starWarsConsumer.getSingleVehicle(vehicleId)));

}

// TASK: Get starships data from starWarsConsumer and parse them to get the name of each starship with starWarsParser, then show the result in the console. TODO: **DEV 1**
async function viewStarshipsNames() {
  const starships = await starWarsConsumer.getStarships();
  const starshipsName = await starWarsParser.getStarshipsNames(starships);
  console.log(starshipsName);
}
// TASK: Get single starship data from starWarsConsumer and parse it to get its manufacturer with starWarsParser, then show the result in the console. TODO: **DEV 2**
async function viewSingleStarshipManufacturer(starshipId) {
  const starship = await starWarsConsumer.getSingleStarship(starshipId)
  const starshipAndManufacturer = starWarsParser.getSingleStarshipManufacturer(starship)
  console.log(`nome: ${starshipAndManufacturer.name}`)
  console.log(`Manufacturer: ${starshipAndManufacturer.manufacturer}`)
}

const starWarsView = {
  viewPlanetsName,
  viewPlanetsRotationPeriod,
  viewSinglePlanetTerrain,
  viewPeopleNames,
  viewPeopleHeights,
  viewSinglePersonHairColor,
  viewFilmsTitles,
  viewSingleFilmOpeningCrawl,
  viewSpeciesNames,
  viewSingleSpeciesLanguage,
  viewVehiclesNames,
  viewSingleVehicleModel,
  viewStarshipsNames,
  viewSingleStarshipManufacturer,
};

export default starWarsView;
