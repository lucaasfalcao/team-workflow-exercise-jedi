// LIST OF PARSER FUNCTIONS TO GET SPECIFIC DATA FROM THE STAR WARS API CONSUMER.

// TASK: Parse the star wars planets data to get the name of each planet. TODO: **DEV 1**
function getPlanetsNames(planets) {
  let planetsNamesList = [];
  planets.forEach(element => {
    let planetName = element.name;
    planetsNamesList.push(planetName);
  })
  return planetsNamesList;
}
   

// TASK: Parse the star wars planets data to get the rotation period of each planet. TODO: **DEV 2**
function getPlanetsRotationPeriod(planets) {
  let listOfRotationPeriod = []
  planets.forEach(element => {
    let tempObj = {}
    tempObj.name = element.name
    tempObj.rotation_period = element.rotation_period
    listOfRotationPeriod.push(tempObj)
  });
  return listOfRotationPeriod
}

// TASK: Parse the star wars single planet data to get its terrain. TODO: **DEV 3**
function getSinglePlanetTerrain(planet) {
  let singlePlanetData = {};
  singlePlanetData.name = planet.name;
  singlePlanetData.terrain = planet.terrain;
  return singlePlanetData
}

// TASK: Parse the star wars people data to get the name of each person. TODO: **DEV 4**
function getPeopleNames(people) {
  let peopleName = [];
  people.foreach(element => {
    let tempObj = {}
    tempObj.name = element.name
    peopleName.push(element.name);
  });
  return peopleName;
}

// TASK: Parse the star wars people data to get the height of each person. TODO: **DEV 1**
function getPeopleHeights(people) {
  let peopleHeightsList = [];
  people.forEach(people => {
    let peopleHight = people.height;
    peopleHeightsList.push(peopleHight);
  })
  return peopleHeightsList;
}
// TASK: Parse the star wars single person data to get its hair color. TODO: **DEV 2**
function getSinglePersonHairColor(person) {
  let ObjOfPersonAndHair = {}
  ObjOfPersonAndHair.name = person.name
  ObjOfPersonAndHair.hairColor = person.hair_color
  return ObjOfPersonAndHair
}
// TASK: Parse the star wars films data to get the title of each film. TODO: **DEV 3**
function getFilmsTitles(films) {
  let filmsTitlesList = [];
  films.forEach(element => {
    let filmTitle = element.title;
    filmsTitlesList.push(filmTitle);
  })
  return filmsTitlesList
}

// TASK: Parse the star wars single film data to get its opening crawl. TODO: **DEV 4**
function getSingleFilmOpeningCrawl(film) {
  return film.opening_crawl;
}

// TASK: Parse the star wars species data to get the name of each species. TODO: **DEV 1**
function getSpeciesNames(species) {
  let speciesNamesList = [];
  species.forEach(element => {
    let speciesName = element.name;
    speciesNamesList.push(speciesName);
  })
  return speciesNamesList;
}
 
// TASK: Parse the star wars single species data to get its language. TODO: **DEV 2**
function getSingleSpeciesLanguage(specie) {
  let objSpeciesAndLanguage = {}
  objSpeciesAndLanguage.name = specie.name
  objSpeciesAndLanguage.language = specie.language
  return objSpeciesAndLanguage
}

// TASK: Parse the star wars vehicles data to get the name of each vehicle. TODO: **DEV 3**
function getVehiclesNames(vehicles) {
  let vehiclesNamesList = [];
  vehicles.forEach(element => {
    let vehicleName = element.name;
    vehiclesNamesList.push(vehicleName);
  })
  return vehiclesNamesList
}

// TASK: Parse the star wars single vehicle data to get its model. TODO: **DEV 4**
function getSingleVehicleModel(vehicle) {
  return vehicle.model;
}

// TASK: Parse the star wars starships data to get the name of each starship. TODO: **DEV 1**
function getStarshipsNames(starships) {
  let starshipsNamesList = [];
  starships.forEach(element => {
    starshipsNamesList.push(element.name);
  })
  return starshipsNamesList;
}

// TASK: Parse the star wars single starship data to get its manufacturer. TODO: **DEV 2**
function getSingleStarshipManufacturer(starship) {
  let objStarshipManufacturer = {}
  objStarshipManufacturer.name = starship.name
  objStarshipManufacturer.manufacturer = starship.manufacturer
  return objStarshipManufacturer
}

const starWarsParser = {
  getPlanetsNames,
  getPlanetsRotationPeriod,
  getSinglePlanetTerrain,
  getPeopleNames,
  getPeopleHeights,
  getSinglePersonHairColor,
  getFilmsTitles,
  getSingleFilmOpeningCrawl,
  getSpeciesNames,
  getSingleSpeciesLanguage,
  getVehiclesNames,
  getSingleVehicleModel,
  getStarshipsNames,
  getSingleStarshipManufacturer,
};

export default starWarsParser;
