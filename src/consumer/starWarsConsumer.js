import axios from "axios";

// STAR WARS API CONSUMPTION

// TASK: Consume star wars API to get planets data. - TODO: **DEV 1**
async function getPlanets() {
    let arrayOfPlanets = []
    for (let i=1;i<=6;i++){
      let getJson = await axios.get(`https://swapi.dev/api/planets/?page=${i}`)
      let planetData = await getJson.data
      planetData.results.forEach(element => {
        arrayOfPlanets.push(element)
      });
    }
    return arrayOfPlanets;
  }


// TASK: Consume star wars API to get people data. - TODO: **DEV 2**
async function getPeople() {
  let arrayOfPeople = []
  for (let i=1;i<=9;i++){
    let getJson = await axios.get(`https://swapi.dev/api/people/?page=${i}`)
    let peopleData = await getJson.data
    peopleData.results.forEach(element => {
      arrayOfPeople.push(element)
    });
  }
  return arrayOfPeople;
}

// TASK: Consume star wars API to get films data. - TODO: **DEV 3**
async function getFilms() {
  const filmsJson = await axios.get('https://swapi.dev/api/films');
  const filmsData = await filmsJson.data.results
  return filmsData;
}

// TASK: Consume star wars API to get species data. - TODO: **DEV 4**

async function getSpecies() {

  let arrayOfSpecies = [];

  // walk through a vector of species, page by page number, looking for all species in API path

  for (let i = 1; i <= 4; i++) {
    let jsonConsumer = await axios.get(`https://swapi.dev/api/species/?page=${i}`);
    await jsonConsumer.data.results.forEach(element => {
      arrayOfSpecies.push(element);
    });
    }
    return arrayOfSpecies;
}



// TASK: Consume star wars API to get vehicles data. - TODO: **DEV 1**
async function getVehicles() {
    let arrayOfVehicles = []
    for (let i=1;i<=4;i++){
      let getJson = await axios.get(`https://swapi.dev/api/vehicles/?page=${i}`)
      let vehicleData = await getJson.data
      vehicleData.results.forEach(element => {
        arrayOfVehicles.push(element)
      });
    }
    return arrayOfVehicles;
}

// TASK: Consume star wars API to get starships data. - TODO: **DEV 2**
async function getStarships() {
  let arrayOfStarships = []
  for (let i=1;i<=4;i++){
    let getJson = await axios.get(`https://swapi.dev/api/starships/?page=${i}`)
    let starshipsData = await getJson.data
    starshipsData.results.forEach(element => {
      arrayOfStarships.push(element)
    });
  }
  return arrayOfStarships;
}

// TASK: Consume star wars API to get a single planet data. - TODO: **DEV 3**
async function getSinglePlanet(id) {
  return (await axios.get(`https://swapi.dev/api/planets/${id}`)).data;
}

// TASK: Consume star wars API to get a single person data. - TODO: **DEV 4**
async function getSinglePerson(id) {

  return (await axios.get(`https://swapi.dev/api/people/${id}`)).data;

}

// TASK: Consume star wars API to get a single film data. - TODO: **DEV 1**
async function getSingleFilm(id) {
    return (await axios.get(`https://swapi.dev/api/films/${id}`)).data;
}
  

// TASK:  Consume star wars API to get a single species data. - TODO: **DEV 2**
async function getSingleSpecies(id) {
  return (await axios.get(`https://swapi.dev/api/species/${id}`)).data;
}

// TASK: Consume star wars API to get a single vehicle data. - TODO: **DEV 3**
async function getSingleVehicle(id) {
  return (await axios.get(`https://swapi.dev/api/vehicles/${id}`)).data;
}

// TASK: Consume star wars API to get a single starship data. - TODO: **DEV 4**
async function getSingleStarship(id) {

  return (await axios.get(`https://swapi.dev/api/starships/${id}`)).data;

}


const starWarsConsumer = {
  getPlanets,
  getPeople,
  getFilms,
  getSpecies,
  getVehicles,
  getStarships,
  getSinglePlanet,
  getSinglePerson,
  getSingleFilm,
  getSingleSpecies,
  getSingleVehicle,
  getSingleStarship,
};

export default starWarsConsumer
