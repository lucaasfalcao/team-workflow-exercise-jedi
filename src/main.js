import starWarsView from "./modules/starWarsView.js";

//Executes all star wars view functions.
export default function main() {
  console.log("REQUESTS: \n");

  starWarsView.viewPlanetsName();

  starWarsView.viewPlanetsRotationPeriod(); 

  starWarsView.viewSinglePlanetTerrain(1); 

  starWarsView.viewPeopleNames();  

  starWarsView.viewPeopleHeights(); 

  starWarsView.viewSinglePersonHairColor(1); 

  starWarsView.viewFilmsTitles(); 

  starWarsView.viewSingleFilmOpeningCrawl(4); 

  starWarsView.viewSpeciesNames(); 

  starWarsView.viewSingleSpeciesLanguage(7); 

  starWarsView.viewVehiclesNames(); 

  starWarsView.viewSingleVehicleModel(6); 

  starWarsView.viewStarshipsNames(); 

  starWarsView.viewSingleStarshipManufacturer(5); 
}
